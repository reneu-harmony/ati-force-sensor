
# ATI Force Sensor codes Repository

## Folder structure

-   applications : Contains the applications
-   esmacat_library : Contains the source/header files for the Esmacat master to communicate with slaves
-   esmacat_slave_drivers : Contains the source/header files for all Esmacat slaves. Can also be used to generate source files to communicate with other EtherCAT slaves.
-   ethercat_driver :  Simple Open-Source EtherCAT Master (SOEM) Open Source library that supports the EtherCAT Master
-   resources: Contains external resources used in the code
-   calibration_files: contains calibration files for the ATI force sensors


## Getting started - Compilation

### Windows

1.  Install
    1.  winpcap ([https://www.winpcap.org/install/](https://www.winpcap.org/install/))
    2.  cmake
    3.  git
    4.  MS visual Studio
2.  Download the source file or git clone [https://bitbucket.org/harmonicbionics/esmacat_master_software.git](https://bitbucket.org/harmonicbionics/esmacat_master_software.git)
3.  Open esmacatserver folder with Visual Studio
4.  Build the project

### Linux

1.  Install build-essential, git and cmake. (e.g., sudo apt-get install build-essential git cmake)
2.  git clone [https://bitbucket.org/harmonicbionics/esmacat_master_software.git](https://bitbucket.org/reneu-harmony/ati-force-sensor.git)
3.  cd ati-force-sensor
4.  mkdir build
5.  cd build
6.  cmake ..
7.  make

## Run read_sensor_mv

1. cd applications
2. cd read_sensor_mv
3. sudo ./read_sensor_mv <gain_c0_c7> <gain_c8_c15>

## Run read_force

1. cd applications
2. cd read_force
3. sudo ./read_force <sensor_code>
