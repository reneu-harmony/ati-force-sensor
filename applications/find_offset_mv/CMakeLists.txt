file(GLOB FIND_OFFSET_MV_HEADERS *.h)
file(GLOB FIND_OFFSET_MV_SOURCES *.cpp *.c)

add_executable(find_offset_mv ${FIND_OFFSET_MV_SOURCES} ${FIND_OFFSET_MV_HEADERS})
target_link_libraries(find_offset_mv ethercat_driver esmacat)
install(TARGETS find_offset_mv DESTINATION ./bin)
