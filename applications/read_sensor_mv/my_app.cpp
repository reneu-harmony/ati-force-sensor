/** @file
 * @brief This file contains the definition of the functions associated with the user-defined
 * application for the Esmacat slave project */
 /*****************************************************************************************
 * INCLUDES
 ****************************************************************************************/
#include "my_app.h"

/*****************************************************************************************
 * FUNCTIONS
 ****************************************************************************************/
/**
 * @brief Identifies the actual Esmacat slave sequence in the EtherCAT communication chain.
 */
void my_app::assign_slave_sequence(){

    if(index == 1) assign_esmacat_slave_index(&ecat_loadcell,0);
    else if(index == 2) assign_esmacat_slave_index(&ecat_loadcell,1);
    //configure_slaves();

}

/**
 * @brief Configure your Esmacat slave.
 * Link Esmacat slave object with the actual Esmacat slave in the EtherCAT communication chain.
 * Functions beginning with 'configure_slave' must only be executed in this function
 */
void my_app::configure_slaves(){
    esmacat_loadcell_interface_channel_config_T config_loadcell;
    config_loadcell.single_ended_diff_ch_0_1 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_2_3 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_4_5 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_6_7 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_8_9 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_10_11 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_12_13 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_14_15 = DIFFERENTIAL_INPUT;

    config_loadcell.buff_en_ADC_ch_0_7 = true;
    config_loadcell.buff_en_ADC_ch_8_15 = true;

    config_loadcell.PGA_ch_0_7 = gain_0_7;
    config_loadcell.PGA_ch_8_15 = gain_8_15;

    ecat_loadcell.configure_slave_loadcell_interface_adc(config_loadcell);
    // add initialization code here
    // Functions starting with "configure_slave" work only in configure_slaves() function
}

/** @brief Initialization that needs to happen on the first iteration of the loop
 */
void my_app::init()
{


}

/**
 * @brief Executes functions at the defined loop rate
 */
void my_app::loop(){
    // add functions below that are to be executed at the loop rate

    cout << "t= " << elapsed_time_ms << "\t";

    cout << "g0: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(8)) << " mV \t";
    cout << "g1: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(10)) << " mV \t";
    cout << "g2: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(0)) << " mV \t";
    cout << "g3: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(2)) << " mV \t";
    cout << "g4: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(4)) << " mV \t";
    cout << "g5: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(6)) << " mV \t";

    cout << endl;

    if (loop_cnt > 100000)
    {
        stop();
    }
}

void my_app::set_gains(int index_local, int g_0_7, int g_8_15)
{
    index = index_local;

    switch(g_0_7)
    {
        case 1: gain_0_7 = PGA1; break;
        case 2: gain_0_7 = PGA2; break;
        case 4: gain_0_7 = PGA4; break;
        case 8: gain_0_7 = PGA8; break;
        case 16: gain_0_7 = PGA16; break;
        case 32: gain_0_7 = PGA32; break;
        case 64: gain_0_7 = PGA64; break;
        default: gain_0_7 = PGA1; break;
    }
    switch(g_8_15)
    {
        case 1: gain_8_15 = PGA1; break;
        case 2: gain_8_15 = PGA2; break;
        case 4: gain_8_15 = PGA4; break;
        case 8: gain_8_15 = PGA8; break;
        case 16: gain_8_15 = PGA16; break;
        case 32: gain_8_15 = PGA32; break;
        case 64: gain_8_15 = PGA64; break;
        default: gain_8_15 = PGA1; break;
    }
}
