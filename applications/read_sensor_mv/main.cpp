/** @file
 *  @brief This file contains the template for the main program for the Esmacat slave
 *  project */
/*****************************************************************************************
 * INCLUDES
 ****************************************************************************************/
#include <iostream>
#include "my_app.h"

/*****************************************************************************************
 * FUNCTIONS
 ****************************************************************************************/
/**
 * @brief Initializes the execution of the Ethercat communication and
 *        primary real-time loop for your application for the desired
 *        slave
 * @return
 */

int main(int argc, char *argv[])
{

    if(argc < 4)
    {
        std::cout << "Usage: sudo ./read_sensor_mv <index> <gain_0_7> <gain_8_15>\n";
        return 0;
    }

    my_app app;

    int index = stoi(argv[1]);
    int input_gain_0_7 = stoi(argv[2]);
    int input_gain_8_15 = stoi(argv[3]);

    if(!(input_gain_0_7==1||input_gain_0_7==2||input_gain_0_7==4||input_gain_0_7==8||input_gain_0_7==16||input_gain_0_7==32||input_gain_0_7==64)||
       !(input_gain_8_15==1||input_gain_8_15==2||input_gain_8_15==4||input_gain_8_15==8||input_gain_8_15==16||input_gain_8_15==32||input_gain_8_15==64))
    {
        std::cout << "Gains selected are invalid. Current options: 1, 2, 4, 8, 16, 32, 64\n";
        return 0;
    }

    app.set_gains(index, input_gain_0_7,input_gain_8_15);

    app.set_ethercat_adapter_name("enp1s0");

    // start the esmacat application customized for your slave
    app.start();

    //the application runs as long as the esmacat master and slave are in communication
    while (app.is_esmacat_master_closed() == FALSE );
    return 0;
}

