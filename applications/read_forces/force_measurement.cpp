#include "force_measurement.h"

void ForceSensor::SetCalibMatrix(Eigen::MatrixXd raw_calib_matrix, Eigen::MatrixXd true_amp_gains, float excitation)
{
    calibration_matrix_ = Eigen::MatrixXd::Zero(6,6);

    for(int i=0;i<6;i++)
    {
        for(int j=0;j<6;j++)
        {
            calibration_matrix_(i,j) = raw_calib_matrix(i,j)/true_amp_gains(j)/excitation;

        }
    }
}

void ForceSensor::ReadCalibParam(std::string sensor_code)
{

    i_json_data_file calib;
    std::string filename = calib_file_path_prefix + sensor_code + ".json";

    config_file_exception exception;

    exception = calib.parse(filename, 1);
    if(static_cast<int>(exception) < 0)
    {
        std::cout << "Calibration file not found for sensor " << sensor_code << "!\n";
        config_file_exception_handler().throw_exception(exception);
    }

    Eigen::MatrixXd raw_calib_matrix = Eigen::MatrixXd::Zero(6,6);
    Eigen::MatrixXd true_amplifier_gains = Eigen::MatrixXd::Zero(1,6);

    int desired_amplification_gain_A = 0;
    int desired_amplification_gain_B = 0;
    float excitation = 0;

    try{
        raw_calib_matrix = calib.get_object("calibration_parameters").get_matrix<_Matrix>("raw_calib_matrix",6,6).value;
        true_amplifier_gains = calib.get_object("calibration_parameters").get_matrix<_Matrix>("true_amplifier_gains",1,6).value;

        desired_amplification_gain_A = calib.get_object("calibration_parameters").get_scalar<_int>("desired_amplification_gain_A").value;
        desired_amplification_gain_B = calib.get_object("calibration_parameters").get_scalar<_int>("desired_amplification_gain_B").value;


        excitation = calib.get_object("calibration_parameters").get_scalar<_float>("excitation").value;
    }
    catch(config_file_exception e)
    {
        if(static_cast<int>(e) < 0) config_file_exception_handler().throw_exception(e);
    }

    SetCalibMatrix(raw_calib_matrix, true_amplifier_gains, excitation);
    gain_channel_A_ = GetProgrammableGain(desired_amplification_gain_A);
    gain_channel_B_ = GetProgrammableGain(desired_amplification_gain_B);

}

ProgrammableGain ForceSensor::GetProgrammableGain(int value)
{
    ProgrammableGain ret;
    switch(value)
    {
        case 1:
            ret = PGA1;
            break;
        case 2:
            ret = PGA2;
            break;
        case 4:
            ret = PGA4;
            break;
        case 8:
            ret = PGA8;
            break;
        case 16:
            ret = PGA16;
            break;
        case 32:
            ret = PGA32;
            break;
        case 64:
            ret = PGA64;
            break;
        default:
            PLOGE << "Amplification gain " << value << " not found!\n";
            ret = PGA1;
            break;
    }
    return ret;
}

void ForceSensor::Configure()
{
    esmacat_loadcell_interface_channel_config_T config_loadcell;
    config_loadcell.single_ended_diff_ch_0_1 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_2_3 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_4_5 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_6_7 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_8_9 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_10_11 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_12_13 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_14_15 = DIFFERENTIAL_INPUT;

    config_loadcell.buff_en_ADC_ch_0_7 = true;
    config_loadcell.buff_en_ADC_ch_8_15 = true;

    std::string sensor_name = "FT" + std::to_string(sensor_code_);

    ReadCalibParam(sensor_name);

    config_loadcell.PGA_ch_0_7 = gain_channel_A_;
    config_loadcell.PGA_ch_8_15 = gain_channel_B_;

    loadcell_interface->configure_slave_loadcell_interface_adc(config_loadcell);
    IO_Direction digital_dir[7];
    for(int i=0;i<7;i++) digital_dir[i] = IO_INPUT;
    //digital_dir[0] = IO_OUTPUT;
    loadcell_interface->configure_slave_dio_direction(digital_dir);

    //for(int i=0;i<7;i++) loadcell_interface->set_digital_output(i,false);
    
    previous_wrench_reading_= Eigen::MatrixXd::Zero(1,6);
    previous_unfiltered_wrench_reading_= Eigen::MatrixXd::Zero(1,6);
    offset_mv_ = Eigen::MatrixXd::Zero(1,6);
    accumulated_reading_for_calibration_= Eigen::MatrixXd::Zero(1,6);
}

Eigen::MatrixXd ForceSensor::ReadRawSignalMiliVolt()
{
    Eigen::MatrixXd  ret = Eigen::MatrixXd::Zero(1,6);

    ret << loadcell_interface->get_analog_input_mV(8),
           loadcell_interface->get_analog_input_mV(10),
           loadcell_interface->get_analog_input_mV(0),
           loadcell_interface->get_analog_input_mV(2),
           loadcell_interface->get_analog_input_mV(4),
           loadcell_interface->get_analog_input_mV(6);


    return ret;
}

Eigen::MatrixXd ForceSensor::GetWrench()
{
    Eigen::MatrixXd unfiltered_wrench_reading, filtered_wrench_reading;
    Eigen::MatrixXd theoretical_wrench;

    unfiltered_wrench_reading = ((ReadRawSignalMiliVolt()-offset_mv_)/1000)*calibration_matrix_.transpose();

    filtered_wrench_reading = 0.5*(unfiltered_wrench_reading+previous_unfiltered_wrench_reading_)*filt_coeff_+previous_wrench_reading_*(1-filt_coeff_);

    previous_wrench_reading_ = filtered_wrench_reading;
    previous_unfiltered_wrench_reading_ = unfiltered_wrench_reading;

    return filtered_wrench_reading-GetBaselineWrench(Eigen::Matrix3d::Identity());
}


void ForceSensor::CalibrateZero(Eigen::Matrix3d rot_J_in_B)
{

    if(calib_loop_ < CALIB_WINDOW)
    {
        calib_loop_++;
        accumulated_reading_for_calibration_+= ReadRawSignalMiliVolt();
    }
    else
    {
        Eigen::MatrixXd avg_reading;
        Eigen::MatrixXd theoretical_wrench, theoretical_reading;

        avg_reading = Eigen::MatrixXd::Zero(1,6);

        avg_reading = accumulated_reading_for_calibration_/CALIB_WINDOW;
        theoretical_wrench = GetBaselineWrench(rot_J_in_B);
        theoretical_reading = theoretical_wrench*((calibration_matrix_.transpose()).inverse())*1000; //mV

        offset_mv_ = avg_reading-theoretical_reading;
        accumulated_reading_for_calibration_= Eigen::MatrixXd::Zero(1,6);
        calibrated_ = true;
        calib_loop_ = 0;
    }

}

Eigen::MatrixXd ForceSensor::GetBaselineWrench(Eigen::Matrix3d rot_J_in_B)
{
    Eigen::MatrixXd baseline_wrench;
    Eigen::Vector3d gravity_vector = translation(-1,'z')*gravity;

    Eigen::Vector3d forces, torques;

    Eigen::Vector3d mount_cg_meters = mount_cg_/1000;
    float mount_mass_kg = mount_mass_/1000;

    baseline_wrench = Eigen::MatrixXd::Zero(1,6);
    forces = mount_mass_kg*rot_S_in_J_.transpose()*rot_J_in_B.transpose()*gravity_vector;
    torques = mount_cg_meters.cross(mount_mass_kg*rot_S_in_J_.transpose()*rot_J_in_B.transpose()*gravity_vector);

    baseline_wrench << forces(0),
                       forces(1),
                       forces(2),
                       torques(0),
                       torques(1),
                       torques(2);

    return baseline_wrench;

}


/** @brief Method to load force sensor parameters from config file
 *
 */
config_file_exception load_force_sensor_parameters(ForceSensor* sensor, const std::string sensor_id,const std::string filename)
{

    config_file_exception no_error = config_file_exception::SUCCESS;
    config_file_exception error;

    force_sensor_config_handling config_parameters;

    config_file_exception exception_loading = config_parameters.load_and_set_parameters(sensor,sensor_id,filename);
    if(static_cast<int>(exception_loading) < 0)
    {
        error = exception_loading;
        return error;
    }

    return no_error;
}



/** @brief Loads force sensor parameters from config files and set locally
 *
 * @param force sensor object receiving parameters
 * @param identifier of the sensor
 * @param filename for exercises parameters
 * @return handle describing the status of operation (error: <0, success: 0, warning >0)
 */
config_file_exception force_sensor_config_handling::load_and_set_parameters(ForceSensor* sensor,const std::string sensor_id,const std::string filename)
{
    //Loading parameters and checking for exceptions
    config_file_exception exception_load_parameters = load_parameters(filename);
    if(static_cast<int>(exception_load_parameters) < 0) return config_file_exception::ERR_FILE_PARSING;

    //Setting parameters and checking for exceptions
    config_file_exception exception_set_parameters  = set_parameters(sensor,sensor_id);
    if(static_cast<int>(exception_set_parameters) < 0) return config_file_exception::ERR_SETTING_PARAM;

    return config_file_exception::SUCCESS;
}

/** @brief Loads force sensor parameters from config files
 *
 * @param filename for exercises parameters
 * @return handle describing the status of operation (error: <0, success: 0, warning >0)
 */
config_file_exception force_sensor_config_handling::load_parameters(const std::string filename)
{
    return parameters.parse(filename);
}

/** @brief Sets exercises parameters (centers, amplitudes, impedances and shr impedances) locally
 *
 * It loads and sets as many exercises as input in the file. This allows easy expansion and access of the library.
 * @param force sensor object receiving parameters
 * @param identifier of the sensor
 * @return handle describing the status of operation (error: <0, success: 0, warning >0)
 */
config_file_exception force_sensor_config_handling::set_parameters(ForceSensor* sensor,const std::string sensor_id)
{
    config_file_exception no_error = config_file_exception::SUCCESS;
    config_file_exception error;

    int sensor_code = 0;
    float mount_mass = 0;
    Eigen::Vector3d mount_cg;   //in local/sensor frame
    Eigen::MatrixXd rot_S_in_J; //rotation matrix from joint frame to sensor frame
    char c_side;
    int side;
    int ref_joint_index = 0;
    float filt_coeff = 0;

    try {
        json_object sensor_param = parameters.get_object(sensor_id);

        sensor_code = sensor_param.get_scalar<_int>("sensor_code").value;
        mount_mass = sensor_param.get_scalar<_double>("toolside_mount_mass").value;
        mount_cg = sensor_param.get_vector<_Vector3d>("toolside_mount_cg_in_S").value;
        rot_S_in_J = sensor_param.get_matrix<_Matrix>("rotation_matrix_S_in_J",3,3).value;
        side = sensor_param.get_scalar<_int>("side").value;
        ref_joint_index = sensor_param.get_scalar<_int>("ref_joint_index").value;
        filt_coeff = sensor_param.get_scalar<_double>("filter_constant").value;

        if(side == 0) c_side = 'L';
        else c_side = 'R';

        sensor->SetSensorCode(sensor_code);
        sensor->SetMountMass(mount_mass);
        sensor->SetMountCG(mount_cg);
        sensor->SetRotMatrix(rot_S_in_J);
        sensor->SetSide(c_side);
        sensor->SetRefJointIndex(ref_joint_index);
        sensor->SetFilterCoeff(filt_coeff);

    } catch (config_file_exception  exception) {
        if(static_cast<int>(exception) < 0)
        {
            error = exception;
            return error;
        }
    }

    return no_error;
}




//Returns a translation vector (3x1), given a displacement and an axis ('x','y' or 'z')
Eigen::Vector3d translation(const double d, const char axis)
{
    Eigen::Vector3d ret;
    ret << Eigen::Vector3d::Zero();

    switch(axis)
    {
        case 'x':
            ret(0) = d;
            break;
        case 'y':
            ret(1) = d;
            break;
        case 'z':
            ret(2) = d;
            break;
    }

    return ret;
}
