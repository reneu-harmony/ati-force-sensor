/** @file
 * @brief This file contains the definition of the functions associated with the user-defined
 * application for the Esmacat slave project */
 /*****************************************************************************************
 * INCLUDES
 ****************************************************************************************/
#include "my_app.h"


/*****************************************************************************************
 * FUNCTIONS
 ****************************************************************************************/
/**
 * @brief Identifies the actual Esmacat slave sequence in the EtherCAT communication chain.
 */
void my_app::assign_slave_sequence(){

    assign_esmacat_slave_index(&ecat_loadcell[0],0);
    assign_esmacat_slave_index(&ecat_loadcell[1],1);

    humerus_interface_sensor_.loadcell_interface = &ecat_loadcell[0];
    wrist_interface_sensor_.loadcell_interface = &ecat_loadcell[1];
}

/**
 * @brief Configure your Esmacat slave.
 * Link Esmacat slave object with the actual Esmacat slave in the EtherCAT communication chain.
 * Functions beginning with 'configure_slave' must only be executed in this function
 */
void my_app::configure_slaves(){

    config_file_exception exception_force_sensor_h_parameters = load_force_sensor_parameters(&humerus_interface_sensor_,"humerus_mount",config_f_sensors);
    if(static_cast<int>(exception_force_sensor_h_parameters) < 0) exception_handler.throw_exception(exception_force_sensor_h_parameters);

    config_file_exception exception_force_sensor_w_parameters = load_force_sensor_parameters(&wrist_interface_sensor_,"wrist_mount",config_f_sensors);
    if(static_cast<int>(exception_force_sensor_w_parameters) < 0) exception_handler.throw_exception(exception_force_sensor_w_parameters);

    humerus_interface_sensor_.Configure();
    wrist_interface_sensor_.Configure();

}

/** @brief Initialization that needs to happen on the first iteration of the loop
 */
void my_app::init()
{

    humerus_interface_sensor_.CalibrateZero(Eigen::Matrix3d::Identity());
    wrist_interface_sensor_.CalibrateZero(Eigen::Matrix3d::Identity());

    log_manager.create_file(logger::create_header());
}

/**
 * @brief Executes functions at the defined loop rate
 */
void my_app::loop(){

    if(!humerus_interface_sensor_.IsCalibrated() && !wrist_interface_sensor_.IsCalibrated())
    {
        humerus_interface_sensor_.CalibrateZero(Eigen::Matrix3d::Identity());
        wrist_interface_sensor_.CalibrateZero(Eigen::Matrix3d::Identity());
    }
    else{

        baseline_wrench_humerus_ = humerus_interface_sensor_.GetBaselineWrench(Eigen::Matrix3d::Identity());
        baseline_wrench_wrist_ = wrist_interface_sensor_.GetBaselineWrench(Eigen::Matrix3d::Identity());

        wrench_humerus_ = humerus_interface_sensor_.GetWrench();
        wrench_wrist_ = wrist_interface_sensor_.GetWrench();


        cout << "W_Fx: " << std::fixed << std::setprecision(2) << wrench_wrist_(0) << "\t";
        cout << "W_Fy: " << std::fixed << std::setprecision(2) << wrench_wrist_(1) << "\t";
        cout << "W_Fz: " << std::fixed << std::setprecision(2) << wrench_wrist_(2) << "\t";
        //cout << "W_Tx: " << std::fixed << std::setprecision(2) << wrench_wrist_(3) << "\t";
        //cout << "W_Ty: " << std::fixed << std::setprecision(2) << wrench_wrist_(4) << "\t";
        //cout << "W_Tz: " << std::fixed << std::setprecision(2) << wrench_wrist_(5) << "\t";

        cout << "\t";

        cout << "H_Fx: " << std::fixed << std::setprecision(2) << wrench_humerus_(0) << "\t";
        cout << "H_Fy: " << std::fixed << std::setprecision(2) << wrench_humerus_(1) << "\t";
        cout << "H_Fz: " << std::fixed << std::setprecision(2) << wrench_humerus_(2) << "\t";
        //cout << "H_Tx: " << std::fixed << std::setprecision(2) << wrench_humerus_(3) << "\t";
        //cout << "H_Ty: " << std::fixed << std::setprecision(2) << wrench_humerus_(4) << "\t";
        //cout << "H_Tz: " << std::fixed << std::setprecision(2) << wrench_humerus_(5) << "\t";


        cout << "\n";

        //float n_f = sqrt(pow(wrench_wrist_(0),2)+pow(wrench_wrist_(1),2)+pow(wrench_wrist_(2),2));

        float n_f = sqrt(pow(wrench_humerus_(0),2)+pow(wrench_humerus_(1),2)+pow(wrench_humerus_(2),2));
        float mass = n_f/gravity-0.130;

        //cout << std::fixed << std::setprecision(2) << n_f << endl;
        if(log_manager.get_rows_in_file()>MAX_ROWS_OUTPUT_FILE)
        {
            log_manager.create_file(logger::create_header());
        }

        log_manager.log_data(logger::create_dataline(loop_cnt,wrench_humerus_,wrench_wrist_,baseline_wrench_humerus_,baseline_wrench_wrist_,
                                                     humerus_interface_sensor_.ReadRawSignalMiliVolt(),wrist_interface_sensor_.ReadRawSignalMiliVolt(),
                                                     humerus_interface_sensor_.GetOffsetMiliVolt(),wrist_interface_sensor_.GetOffsetMiliVolt()));

    }
    if (loop_cnt > 5000000)
    {
        stop();
    }
}
