file(GLOB READ_FORCES_HEADERS *.h)
file(GLOB READ_FORCES_SOURCES *.cpp *.c)

add_executable(read_forces ${READ_FORCES_SOURCES} ${READ_FORCES_HEADERS})
target_link_libraries(read_forces ethercat_driver esmacat file_handling)
install(TARGETS read_forces DESTINATION ./bin)
