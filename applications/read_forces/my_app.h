/** @file
 * @brief This file contains the declaration of the class associated with the user-defined
 * application for the Esmacat slave project */

#ifndef MY_APP_H
#define MY_APP_H

/*****************************************************************************************
 * INCLUDES
 ****************************************************************************************/
#include <iostream>
#include "application.h"
#include "logging.h"
#include "force_measurement.h"
#include "plog/Log.h"

#define FT34273 34273
#define FT33234 33234
#define CALIB_TIME 50
#define STABILIZATION_TIME 300

using namespace std;

config_file_exception load_force_sensor_parameters(ForceSensor* sensor, const std::string sensor_id,const std::string filename);

/*****************************************************************************************
 * CLASSES
 ****************************************************************************************/
/**
 * @brief Description of your custom application class
 *
 * Your custom application class my_app inherits the class 'esmacat_application'
 * Write functions to override the parent functions of 'esmacat_application'
 * Declare an object of your slave class (e.g. ecat_ai)
 * Declare any other variables you might want to add
 * Define the constructor for initialization
 */
class my_app : public esmacat_application
{
private:
    void assign_slave_sequence(); /** identify sequence of slaves and their types*/
    void configure_slaves(); /** setup the slave*/
    void init(); /** code to be executed in the first iteration of the loop */
    void loop(); /** control loop*/

    ForceSensor humerus_interface_sensor_;
    ForceSensor wrist_interface_sensor_;

    esmacat_loadcell_interface ecat_loadcell[2];

    csv_log_file log_manager;

    Eigen::MatrixXd wrench_humerus_;
    Eigen::MatrixXd baseline_wrench_humerus_;
    Eigen::MatrixXd wrench_wrist_;
    Eigen::MatrixXd baseline_wrench_wrist_;

    bool calibrated = false;

    config_file_exception_handler exception_handler;

public:
    /** A constructor- sets initial values for class members */
    my_app()
    {

    }

    unsigned sensor_code_1;
    unsigned sensor_code_2;

};

#endif // MY_APP_H
