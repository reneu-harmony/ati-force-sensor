#include "logging.h"

namespace logger{

std::string create_header()
{
    std::stringstream header;
    header.str("");
    header << "Frame" << ",";
    header << "H_Fx,H_Fy,H_Fz,H_Tx,H_Ty,H_Tz,";
    header << "W_Fx,W_Fy,W_Fz,W_Tx,W_Ty,W_Tz,";
    header << "mountH_Fx,mountH_Fy,mountH_Fz,mountH_Tx,mountH_Ty,mountH_Tz,";
    header << "mountW_Fx,mountW_Fy,mountW_Fz,mountW_Tx,mountW_Ty,mountW_Tz,";
    header << "H_G0,H_G1,H_G2,H_G3,H_G4,H_G5,";
    header << "W_G0,W_G1,W_G2,W_G3,W_G4,W_G5,";
    header << "biasH_G0,biasH_G1,biasH_G2,biasH_G3,biasH_G4,biasH_G5,";
    header << "biasW_G0,biasW_G1,biasW_G2,biasW_G3,biasW_G4,biasW_G5";

    return header.str();

}

std::string create_dataline(const unsigned loop_cnt, Eigen::MatrixXd hum_wrench, Eigen::MatrixXd wrist_wrench,
                            Eigen::MatrixXd base_hum_wrench, Eigen::MatrixXd base_wrist_wrench,
                            Eigen::MatrixXd hum_raw, Eigen::MatrixXd wrist_raw,
                            Eigen::MatrixXd hum_bias, Eigen::MatrixXd wrist_bias)
{
    std::stringstream header;
    header.str("");

    header << loop_cnt << ",";


    for(int i=0;i<6;i++)
        header << hum_wrench(i) << ",";

    for(int i=0;i<6;i++)
        header << wrist_wrench(i) << ",";

    for(int i=0;i<6;i++)
        header << base_hum_wrench(i) << ",";

    for(int i=0;i<6;i++)
        header << base_wrist_wrench(i) << ",";

    for(int i=0;i<6;i++)
        header << hum_raw(i) << ",";

    for(int i=0;i<6;i++)
        header << wrist_raw(i) << ",";

    for(int i=0;i<6;i++)
        header << hum_bias(i) << ",";

    for(int i=0;i<5;i++)
        header << wrist_bias(i) << ",";

    header << wrist_bias(5);

    return header.str();
}

}

