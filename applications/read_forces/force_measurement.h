#ifndef FORCE_MEASUREMENT_H
#define FORCE_MEASUREMENT_H

#include "loadcell_interf.h"
#include "json_handling.h"
#include <Eigen/Dense>

#define CALIB_WINDOW 100



/** @brief Filenames for calib files*/
static const std::string calib_file_path_prefix = "calibration_files/";
static const std::string config_f_sensors = calib_file_path_prefix + "F_sensors_parameters.json";

/** @brief Gravity acceleration*/
static const double gravity = 9.81;

Eigen::Vector3d translation(const double d, const char axis);

class ForceSensor
{
private:

    void ReadCalibParam(std::string sensor_code);
    void SetCalibMatrix(Eigen::MatrixXd raw_calib_matrix,Eigen::MatrixXd true_amp_gains, float excitation);
    ProgrammableGain GetProgrammableGain(int value);

    int sensor_code_ = 0;
    float mount_mass_ = 0;       //grams
    Eigen::Vector3d mount_cg_;   //w.r.t sensor frame (milimeters)
    Eigen::Matrix3d rot_S_in_J_; //rotation matrix from joint frame to sensor frame
    char side_ = 'L';
    int ref_joint_index_ = 0;    //joint frame for reference

    Eigen::MatrixXd calibration_matrix_;
    Eigen::MatrixXd offset_mv_;

    Eigen::MatrixXd accumulated_reading_for_calibration_;
    Eigen::MatrixXd previous_wrench_reading_;
    Eigen::MatrixXd previous_unfiltered_wrench_reading_;

    ProgrammableGain gain_channel_A_;
    ProgrammableGain gain_channel_B_;

    float filt_coeff_ = 1;
    int calib_loop_ = 0;
    bool calibrated_ = false;


public:
    ForceSensor(){};

    void SetSensorCode(int input){sensor_code_=input;}
    void SetMountMass(float input){mount_mass_=input;}
    void SetMountCG(Eigen::Vector3d input){mount_cg_=input;}
    void SetRotMatrix(Eigen::Matrix3d input){rot_S_in_J_=input;}
    void SetSide(char input){side_ = input;}
    void SetRefJointIndex(int input){ref_joint_index_=input;}
    void SetFilterCoeff(float input){filt_coeff_=input;}

    int GetSensorCode(){return sensor_code_;}
    char GetSide(){return side_;}
    int GetRefJointIndex(){return ref_joint_index_;}
    bool IsCalibrated(){return calibrated_;}

    Eigen::MatrixXd GetOffsetMiliVolt(){return offset_mv_;}

    Eigen::MatrixXd ReadRawSignalMiliVolt();
    void Configure();
    void CalibrateZero(Eigen::Matrix3d rot_J_in_B);
    Eigen::MatrixXd GetWrench(); //in local coordinate system
    Eigen::MatrixXd GetBaselineWrench(Eigen::Matrix3d rot_J_in_B); //in local coordinate system


    esmacat_loadcell_interface* loadcell_interface;

};

class force_sensor_config_handling
{

public:
    force_sensor_config_handling(){}
    config_file_exception load_and_set_parameters(ForceSensor*, const std::string, const std::string);

private:

    config_file_exception load_parameters(const std::string);
    config_file_exception set_parameters(ForceSensor*,const std::string);

    i_json_data_file parameters;
};


#endif // FORCE_MEASUREMENT_H
