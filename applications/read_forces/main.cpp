/** @file
 *  @brief This file contains the template for the main program for the Esmacat slave
 *  project */
/*****************************************************************************************
 * INCLUDES
 ****************************************************************************************/
#include <iostream>
#include "my_app.h"

/*****************************************************************************************
 * FUNCTIONS
 ****************************************************************************************/
/**
 * @brief Initializes the execution of the Ethercat communication and
 *        primary real-time loop for your application for the desired
 *        slave
 * @return
 */

int main(int argc, char *argv[])
{

/*
    if(argc < 3)
    {
        std::cout << "Usage: sudo ./read_forces <sensor_code_1> <sensor_code_2>\n";
        return 0;
    }*/

    my_app app;
/*
    if(strcmp(argv[1],"FT34273")==0) app.sensor_code_1 = FT34273;
    else if(strcmp(argv[1],"FT33234")==0) app.sensor_code_1 = FT33234;
    else
    {
        std::cout << "Sensor code 1 selected is invalid. Current options: FT33234 and FT34273\n";
        return 0;
    }
    if(strcmp(argv[2],"FT34273")==0) app.sensor_code_2 = FT34273;
    else if(strcmp(argv[2],"FT33234")==0) app.sensor_code_2 = FT33234;
    else
    {
        std::cout << "Sensor code 2 selected is invalid. Current options: FT33234 and FT34273\n";
        return 0;
    }*/

    app.set_ethercat_adapter_name("enp1s0");

    // start the esmacat application customized for your slave
    app.start();

    //the application runs as long as the esmacat master and slave are in communication
    while (app.is_esmacat_master_closed() == FALSE );
    return 0;
}

