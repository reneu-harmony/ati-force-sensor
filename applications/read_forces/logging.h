#ifndef LOGGING_H
#define LOGGING_H

#include "csv_handling.h"
#include <Eigen/Dense>

namespace logger{
std::string create_header();
std::string create_dataline(const unsigned loop_cnt, Eigen::MatrixXd hum_wrench, Eigen::MatrixXd wrist_wrench,
                            Eigen::MatrixXd base_hum_wrench, Eigen::MatrixXd base_wrist_wrench,
                            Eigen::MatrixXd hum_raw, Eigen::MatrixXd wrist_raw,
                            Eigen::MatrixXd hum_bias, Eigen::MatrixXd wrist_bias);
}


#endif // LOGGING_H
