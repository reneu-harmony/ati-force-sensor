/** @file
 * @brief This file contains the definition of the functions associated with the user-defined
 * application for the Esmacat slave project */
 /*****************************************************************************************
 * INCLUDES
 ****************************************************************************************/
#include "my_app.h"

/*****************************************************************************************
 * FUNCTIONS
 ****************************************************************************************/
/**
 * @brief Identifies the actual Esmacat slave sequence in the EtherCAT communication chain.
 */
void my_app::assign_slave_sequence(){

    if(index == 1) assign_esmacat_slave_index(&ecat_loadcell,0);
    else if(index == 2) assign_esmacat_slave_index(&ecat_loadcell,1);
    //configure_slaves();

}

/**
 * @brief Configure your Esmacat slave.
 * Link Esmacat slave object with the actual Esmacat slave in the EtherCAT communication chain.
 * Functions beginning with 'configure_slave' must only be executed in this function
 */
void my_app::configure_slaves(){
    
    config_loadcell.single_ended_diff_ch_0_1 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_2_3 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_4_5 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_6_7 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_8_9 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_10_11 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_12_13 = DIFFERENTIAL_INPUT;
    config_loadcell.single_ended_diff_ch_14_15 = DIFFERENTIAL_INPUT;

    config_loadcell.buff_en_ADC_ch_0_7 = true;
    config_loadcell.buff_en_ADC_ch_8_15 = true;

    set_gains(gain_0_7,gain_8_15);
}

/** @brief Initialization that needs to happen on the first iteration of the loop
 */
void my_app::init()
{


}

/**
 * @brief Executes functions at the defined loop rate
 */
void my_app::loop(){
    // add functions below that are to be executed at the loop rate
    double input_voltage = 1;
    double temp;

    if(stabilized)
    {
        if(loop_cnt%100==0)
        {

            cout << "Sampled amplified data! Enter input voltage: ";

            cin >> input_voltage;
            if(input_voltage==0) input_voltage=1;

            buffer_gain = buffer_gain/99;


            for(int i=0;i<6;i++) actual_gain(count,i) = buffer_gain(i)/input_voltage;

            cout << "g0: " << std::fixed << std::setprecision(2) << (actual_gain(count,0)) << " \t";
            cout << "g1: " << std::fixed << std::setprecision(2) << (actual_gain(count,1)) << " \t";
            cout << "g2: " << std::fixed << std::setprecision(2) << (actual_gain(count,2)) << " \t";
            cout << "g3: " << std::fixed << std::setprecision(2) << (actual_gain(count,3)) << " \t";
            cout << "g4: " << std::fixed << std::setprecision(2) << (actual_gain(count,4)) << " \t";
            cout << "g5: " << std::fixed << std::setprecision(2) << (actual_gain(count,5)) << " \t";

            cout << endl;

            buffer_gain << 0,0,0,0,0,0;
            count++;
            cout << "Change input voltage and press ENTER. ";
            getchar();
            getchar();

            stabilized = false;

        }
        else{
            new_value << ecat_loadcell.get_analog_input_mV(8),
                         ecat_loadcell.get_analog_input_mV(10),
                         ecat_loadcell.get_analog_input_mV(0),
                         ecat_loadcell.get_analog_input_mV(2),
                         ecat_loadcell.get_analog_input_mV(4),
                         ecat_loadcell.get_analog_input_mV(6);

            buffer_gain+=new_value;

        }
    }
    cout << "t= " << elapsed_time_ms << "\t";

    cout << "g0: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(8)) << " mV \t";
    cout << "g1: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(10)) << " mV \t";
    cout << "g2: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(0)) << " mV \t";
    cout << "g3: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(2)) << " mV \t";
    cout << "g4: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(4)) << " mV \t";
    cout << "g5: " << std::fixed << std::setprecision(2) << (ecat_loadcell.get_analog_input_mV(6)) << " mV \t";

    cout << endl;

    if ((loop_cnt > 100000))
    {
        stop();
    }
    else if ((count>=MAX_INSTANCES))
    {
        for(int i=0;i<6;i++)
        {
            for(int j=0;j<MAX_INSTANCES;j++)
            {
                mean_actual_gain(i) += actual_gain(j,i);
            }
            mean_actual_gain(i) = mean_actual_gain(i)/(MAX_INSTANCES);
        }

        cout << "All gains: " << actual_gain << endl << endl;

        cout << "Actual gains: \n";
        cout << mean_actual_gain(0) << "," << mean_actual_gain(1) << "," << mean_actual_gain(2) << "," <<
                mean_actual_gain(3) << "," << mean_actual_gain(4) << "," << mean_actual_gain(5) << "," << endl;
        stop();
    }


    if((loop_cnt%STABILIZATION_TIME==0)&&!stabilized) stabilized = true;

}

void my_app::set_desired_gains(int index_local, int g_0_7, int g_8_15)
{
    index = index_local;

    switch(g_0_7)
    {
        case 1: gain_0_7 = PGA1; break;
        case 2: gain_0_7 = PGA2; break;
        case 4: gain_0_7 = PGA4; break;
        case 8: gain_0_7 = PGA8; break;
        case 16: gain_0_7 = PGA16; break;
        case 32: gain_0_7 = PGA32; break;
        case 64: gain_0_7 = PGA64; break;
        default: gain_0_7 = PGA1; break;
    }
    switch(g_8_15)
    {
        case 1: gain_8_15 = PGA1; break;
        case 2: gain_8_15 = PGA2; break;
        case 4: gain_8_15 = PGA4; break;
        case 8: gain_8_15 = PGA8; break;
        case 16: gain_8_15 = PGA16; break;
        case 32: gain_8_15 = PGA32; break;
        case 64: gain_8_15 = PGA64; break;
        default: gain_8_15 = PGA1; break;
    }
}

void my_app::set_gains(ProgrammableGain g_0_7, ProgrammableGain g_8_15)
{

    config_loadcell.PGA_ch_0_7 = g_0_7;
    config_loadcell.PGA_ch_8_15 = g_8_15;

    ecat_loadcell.configure_slave_loadcell_interface_adc(config_loadcell);

}
