/** @file
 * @brief This file contains the declaration of the class associated with the user-defined
 * application for the Esmacat slave project */

#ifndef MY_APP_H
#define MY_APP_H

/*****************************************************************************************
 * INCLUDES
 ****************************************************************************************/
#include <iostream>
#include <Eigen/Dense>
#include "application.h"
//Include the header file for the Esmacat slave you plan to use for e.g. Analog Input slave
//#include "esmacat_analog_input.h"
#include "loadcell_interf.h"
#include "plog/Log.h"

#define FT34273 34273
#define FT33234 33234
#define MAX_INSTANCES 4
#define STABILIZATION_TIME 100

using namespace std;


/*****************************************************************************************
 * CLASSES
 ****************************************************************************************/
/**
 * @brief Description of your custom application class
 *
 * Your custom application class my_app inherits the class 'esmacat_application'
 * Write functions to override the parent functions of 'esmacat_application'
 * Declare an object of your slave class (e.g. ecat_ai)
 * Declare any other variables you might want to add
 * Define the constructor for initialization
 */
class my_app : public esmacat_application
{
private:
    void assign_slave_sequence(); /** identify sequence of slaves and their types*/
    void configure_slaves(); /** setup the slave*/
    void init(); /** code to be executed in the first iteration of the loop */
    void loop(); /** control loop*/

    Eigen::MatrixXd buffer_gain = Eigen::MatrixXd::Zero(1,6);
    Eigen::MatrixXd actual_gain = Eigen::MatrixXd::Zero(MAX_INSTANCES,6);
    Eigen::MatrixXd mean_actual_gain = Eigen::MatrixXd::Zero(1,6);

    Eigen::MatrixXd new_value = Eigen::MatrixXd::Zero(1,6);

    int step = 0;

    unsigned count = 0;

    bool stabilized = false;

    esmacat_loadcell_interface_channel_config_T config_loadcell;
    esmacat_loadcell_interface ecat_loadcell;
    //esmacat_analog_input_slave ecat_ai; /**< create your Esmacat slave object */
public:
    /** A constructor- sets initial values for class members */
    my_app()
    {

    }
    void set_desired_gains(int, int, int);
    void set_gains(ProgrammableGain,ProgrammableGain);

    int index = 0;
    ProgrammableGain gain_0_7;
    ProgrammableGain gain_8_15;
};

#endif // MY_APP_H
