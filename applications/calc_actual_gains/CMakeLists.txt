file(GLOB CALC_ACTUAL_GAINS_HEADERS *.h)
file(GLOB CALC_ACTUAL_GAINS_SOURCES *.cpp *.c)

add_executable(calc_actual_gains ${CALC_ACTUAL_GAINS_SOURCES} ${CALC_ACTUAL_GAINS_HEADERS})
target_link_libraries(calc_actual_gains ethercat_driver esmacat)
install(TARGETS calc_actual_gains DESTINATION ./bin)
